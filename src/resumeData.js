import avatar from './images/avatar.jpg';

const ResumeData = {
  myName: 'Igor Nehoroshev',
  avatar: avatar,
  tabLabels: ['Skills', 'Experience'],
  profileSummary: `Software developer since 2013 (started with game development).
                    Working as software developer and networking specialist since 2017.
                    Always trying something new and learning fast.
                    Writing clean and working code, to check it use GitLab.`,

  buttonList: [
    {
      label: 'GitLab',
      link: 'https://gitlab.com/harry.sky.vortex'
    },
    {
      label: 'GitHub',
      link: 'https://github.com/HarrySky'
    },
    {
      label: 'LinkedIn',
      link: 'https://www.linkedin.com/in/igor-nehoroshev'
    },
    {
      label: 'Website',
      link: 'https://neigor.me'
    }
  ],

  skillsList: [
    'REST APIs Integration Experience',
    'Python 3 (Starlette, Flask, Django, Bottle)', 
    'JavaScript/TypeScript (React.js)',
    'C/C++ (Martem AS software)',
    'Docker (Small and Efficient Containers for Projects)',
    'Networking and Servers Handling Experience (Debian, Arch Linux, Ubuntu, pfSense)',
    'OpenVPN/IPSec',
    'PostgreSQL / MongoDB / MySQL',
    'Zabbix monitoring'
  ],

  experienceList: [
    {
      company: 'Martem AS (Software Developer/IT Network Administrator)',
      projects: [
        {
          id: 1,
          value: `Smart Street Light Control`
        },
        {
          id: 2,
          value: `Complete Network Rework`
        },
        {
          id: 3,
          value: `Custom OpenVPN Solutions`
        },
        {
          id: 4,
          value: `Zabbix Monitoring Solutions`
        }
      ]
    },
    {
      company: 'HashDivision OÜ (CEO/Software Developer)',
      projects: [
        {
          id: 1,
          value: `HeadwayFlow (Abandoned Project)`
        }
      ]
    },
    {
      company: 'Worldman Games (Tech Lead)',
      projects: [
        {
          id: 1,
          value: `Custom Timer for Burpee Challenge on Events`
        },
        {
          id: 2,
          value: `Website https://wmgames.ee for Events`
        },
        {
          id: 3,
          value: `Registration and Confirmation for Events`
        },
        {
          id: 4,
          value: `IT System for Events`
        }
      ]
    } // end project object
  ] // end experienceList array
} // end ResumeData

export default ResumeData;